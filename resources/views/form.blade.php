<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Daftar</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <label>first name :</label><br><br>
        <input type="text" name="depan"><br><br>
        <label>Last name :</label><br><br>
        <input type="text" name="belakang"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="ind">Indonesia</option>
            <option value="usa">Amerika Serikat</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>