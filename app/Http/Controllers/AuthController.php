<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('form');
    }

    public function welcome(Request $request)
    {
        $depan = $request['depan'];
        $belakang = $request['belakang'];

        return view('welcome', compact('depan', 'belakang'));
    }
}
